package ru.t1.gorodtsova.tm.api.service.dto;

import ru.t1.gorodtsova.tm.api.repository.dto.IDtoRepository;
import ru.t1.gorodtsova.tm.dto.model.AbstractModelDTO;

public interface IDtoService<M extends AbstractModelDTO> extends IDtoRepository<M> {
}
