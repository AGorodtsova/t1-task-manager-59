package ru.t1.gorodtsova.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.gorodtsova.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.gorodtsova.tm.api.service.dto.IUserOwnedDtoService;
import ru.t1.gorodtsova.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.gorodtsova.tm.exception.entity.ModelNotFoundException;
import ru.t1.gorodtsova.tm.exception.field.IdEmptyException;
import ru.t1.gorodtsova.tm.exception.field.UserIdEmptyException;

import java.util.List;

public abstract class AbstractUserOwnedDtoService
        <M extends AbstractUserOwnedModelDTO, R extends IUserOwnedDtoRepository<M>>
        extends AbstractDtoService<M, R>
        implements IUserOwnedDtoService<M> {

    @NotNull
    @Autowired
    protected IUserOwnedDtoRepository<M> repository;

    @NotNull
    @Transactional
    public M add(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        model.setUserId(userId);
        repository.add(model);
        return model;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model = repository.findOneById(userId, id);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.removeAll(userId);
    }

    @Override
    @Transactional
    public void removeOne(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        if (!userId.equals(model.getUserId())) throw new ModelNotFoundException();
        repository.removeOne(model);
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        findOneById(userId, id);
        repository.removeOneById(userId, id);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(userId, id) != null;
    }

}
