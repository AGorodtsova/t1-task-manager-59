package ru.t1.gorodtsova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAll(@NotNull String userId, @NotNull Comparator<Task> comparator);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}
