package ru.t1.gorodtsova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.gorodtsova.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.gorodtsova.tm.exception.entity.ModelNotFoundException;

public abstract class AbstractUserOwnedDtoRepository
        <M extends AbstractUserOwnedModelDTO>
        extends AbstractDtoRepository<M>
        implements IUserOwnedDtoRepository<M> {

    @NotNull
    @Override
    public M add(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public void removeOne(@NotNull final String userId, @NotNull final M model) {
        if (existsById(userId, model.getId())) entityManager.remove(model);
    }

    @Override
    public void removeOneById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final M model = findOneById(userId, id);
        if (model == null) throw new ModelNotFoundException();
        removeOne(model);
    }

}
