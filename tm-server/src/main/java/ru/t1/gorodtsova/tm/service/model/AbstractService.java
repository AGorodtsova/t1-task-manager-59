package ru.t1.gorodtsova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.gorodtsova.tm.api.repository.model.IRepository;
import ru.t1.gorodtsova.tm.api.service.model.IService;
import ru.t1.gorodtsova.tm.exception.entity.ModelNotFoundException;
import ru.t1.gorodtsova.tm.exception.field.IdEmptyException;
import ru.t1.gorodtsova.tm.model.AbstractModel;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    @Autowired
    protected IRepository<M> repository;

    @NotNull
    @Override
    @Transactional
    public M add(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        repository.add(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> add(@Nullable final Collection<M> models) {
        if (models == null) throw new ModelNotFoundException();
        repository.add(models);
        return models;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> set(@Nullable final Collection<M> models) {
        if (models == null) throw new ModelNotFoundException();
        repository.set(models);
        return models;
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        repository.update(model);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model = repository.findOneById(id);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @Override
    @Transactional
    public void removeAll() {
        repository.removeAll();
    }

    @Override
    @Transactional
    public void removeAll(@NotNull Collection<M> collection) {
        if (collection.isEmpty()) throw new ModelNotFoundException();
        repository.removeAll(collection);
    }

    @Override
    @Transactional
    public void removeOne(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.removeOne(model);
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model;
        model = repository.findOneById(id);
        if (model == null) throw new EntityNotFoundException();
        repository.removeOneById(id);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.findOneById(id) != null;
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

}
